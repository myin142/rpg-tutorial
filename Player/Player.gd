extends KinematicBody2D

export var speed = 80
export var roll_speed = 125
export var acceleration = 500
export var friction = 500

onready var anim_tree = $AnimationTree
onready var anim_state = anim_tree.get("parameters/playback")
onready var sword_hit_box = $HitBoxPivot/SwordHitBox
onready var hurt_box = $HurtBox

enum {
	MOVE,
	ROLL,
	ATTACK
}

const PlayerHurt = preload("res://Player/PlayerHurtSound.tscn")

var state = MOVE
var velocity = Vector2.ZERO
var roll_vector = Vector2.DOWN
var stats := PlayerStats

func _ready():
	randomize()
	stats.connect("no_health", self, "queue_free")
	anim_tree.active = true
	sword_hit_box.knockback_vector = roll_vector

func _physics_process(delta: float) -> void:
	match state:
		MOVE: move_state(delta)
		ROLL: roll_state(delta)
		ATTACK: attack_state(delta)

func move_state(delta: float):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	if input_vector != Vector2.ZERO:
		roll_vector = input_vector
		sword_hit_box.knockback_vector = input_vector
		anim_tree.set("parameters/Idle/blend_position", input_vector)
		anim_tree.set("parameters/Run/blend_position", input_vector)
		anim_tree.set("parameters/Attack/blend_position", input_vector)
		anim_tree.set("parameters/Roll/blend_position", input_vector)
		anim_state.travel("Run")
		velocity = velocity.move_toward(input_vector.normalized() * speed, acceleration * delta)
	else:
		anim_state.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
		
	move()
		
	if Input.is_action_just_pressed("roll"):
		state = ROLL
	
	if Input.is_action_just_pressed("attack"):
		state = ATTACK

func roll_state(delta: float):
	velocity = roll_vector * roll_speed
	anim_state.travel("Roll")
	move()
	
func attack_state(delta: float):
	velocity = Vector2.ZERO
	anim_state.travel("Attack")

func move():
	velocity = move_and_slide(velocity)

func attack_animation_finished():
	state = MOVE
	
func roll_animation_finished():
	velocity /= 2
	state = MOVE


func _on_HurtBox_area_entered(area):
	stats.health -= area.damage
	hurt_box.start_invincibility(.5)
	hurt_box.create_hit_effect()
	var sound = PlayerHurt.instance()
	get_tree().current_scene.add_child(sound)
