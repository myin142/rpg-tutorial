extends Area2D

signal invicibility_started
signal invicibility_ended

const HitEffect = preload("res://Effects/HitEffect.tscn")

onready var timer = $Timer
onready var collision = $CollisionShape2D

var invincible = false setget set_invincible

func set_invincible(value):
	invincible = value
	if invincible == true:
		emit_signal("invicibility_started")
	else:
		emit_signal("invicibility_ended")

func start_invincibility(duration):
	self.invincible = true
	timer.start(duration)

func create_hit_effect():
	var effect = HitEffect.instance()
	get_tree().current_scene.add_child(effect)
	effect.global_position = global_position

func _on_Timer_timeout():
	self.invincible = false

func _on_HurtBox_invicibility_started():
	collision.set_deferred("disabled", true)

func _on_HurtBox_invicibility_ended():
	collision.disabled = false

