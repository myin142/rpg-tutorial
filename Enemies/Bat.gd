extends KinematicBody2D

export var acceleration = 300
export var speed = 50
export var friction = 200
export var wander_target_range = 4

onready var stats = $Stats
onready var detection = $PlayerDetectionZone
onready var sprite = $AnimatedSprite
onready var hurt_box = $HurtBox
onready var soft_collision = $SoftCollision
onready var wander_ctrl = $WanderController

const DeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")

enum {
	IDLE,
	WANDER,
	CHASE,
}

var velocity = Vector2.ZERO
var knockback = Vector2.ZERO
var state = CHASE

func _ready():
	state = pick_random_state([IDLE, WANDER])

func _physics_process(delta: float):
	knockback = knockback.move_toward(Vector2.ZERO, friction * delta)
	knockback = move_and_slide(knockback)
	
	match state:
		IDLE:
			velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
			seek_and_wander()
		WANDER: 
			seek_and_wander()
			move_velocity_towards(wander_ctrl.target_position, delta)
			
			if global_position.distance_to(wander_ctrl.target_position) <= wander_target_range:
				start_new_wander()
		CHASE: 
			var player = detection.player
			if player != null:
				move_velocity_towards(player.global_position, delta)
			else:
				state = IDLE
	
	sprite.flip_h = velocity.x < 0
	if soft_collision.is_colliding():
		velocity += soft_collision.get_push_vector() * delta * 400
	velocity = move_and_slide(velocity)

func move_velocity_towards(pos, delta):
	var dir = global_position.direction_to(pos)
	velocity = velocity.move_toward(dir * speed, acceleration * delta)

func seek_and_wander():
	seek_player()
	if wander_ctrl.get_time_left() == 0:
		start_new_wander()
		
func start_new_wander():
	state = pick_random_state([IDLE, WANDER])
	wander_ctrl.start_wander_timer(rand_range(1, 3))

func seek_player():
	if detection.can_see_player():
		state = CHASE

func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()

func _on_HurtBox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 120
	hurt_box.create_hit_effect()

func _on_Stats_no_health():
	queue_free()
	var effect = DeathEffect.instance()
	get_parent().add_child(effect)
	effect.global_position = global_position
